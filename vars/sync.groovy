import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.api.Logical;
import com.bettercloud.vault.response.LogicalResponse;

import com.cloudbees.plugins.credentials.*;
import com.cloudbees.plugins.credentials.domains.*
import org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl;
import com.cloudbees.plugins.credentials.domains.Domain;
import hudson.util.Secret;
import com.cloudbees.plugins.credentials.SystemCredentialsProvider;

String token = ""

Logical getLogical() {
   def VaultConfig config = new VaultConfig()
                                  .address("http://35.224.33.243:8200")
                                  .token(token)
                                  .build();


   def Vault vault = new Vault(config);
   def Logical logical = vault.logical();
   return logical;
}

ArrayList _getSecretsList(Logical logical, String secretEngine) {
   ArrayList secretList = new ArrayList();
   def temp = logical.list(secretEngine);
   //println(temp.getData().get("keys"));
   if(temp.getData().get("keys")) {
       def list = Eval.me(temp.getData().get("keys"));
       for(iList=0; iList < list.size(); iList++) {
          temp = logical.list(secretEngine + "/" + list[iList]);
          if(temp.getData().get("keys")) {
              def kvL = Eval.me(temp.getData().get("keys"));
              for(int ikvL = 0; ikvL < kvL.size(); ikvL++) {
        	    secretList.add(list[iList]+kvL[ikvL]);
              }
          }
       }
   }
   return secretList;
}

def getSecretsList() {
   def Logical logical = getLogical();
   ArrayList secrets = _getSecretsList(logical, "kv");
   //println(secrets);
   return(secrets);
}

ArrayList<HashMap<String, HashMap<String, String>>> _getSecret(Logical logical, String secretEngine, ArrayList secrets) {
   ArrayList<HashMap<String, HashMap<String, String>>> kvList = new HashMap<>();
   for(int iSecret = 0; iSecret < secrets.size(); iSecret++) {
      def path = secretEngine + "/" + secrets.getAt(iSecret);
      try {
          kvL = logical.read(path).getData();
          def keySets = kvL.keySet();
          for(int iKvl = 0; iKvl < keySets.size(); iKvl++) {
              HashMap<String, HashMap<String, String>> temp = new HashMap<>();
              HashMap<String, String> hash = new HashMap<>();

	      hash.put(keySets[iKvl], kvL.get(keySets[iKvl]));
	      temp.put(secrets.getAt(iSecret), hash);
              kvList.add(temp);
          }
      }
      catch(Exception ex) {
         println("Catching the exception");
         println(ex.getMessage());
      }
   }
   return kvList;
}

def getAllSecrets() {
   def Logical logical = getLogical();
   ArrayList secrets = getSecretsList();
   ArrayList kv = _getSecret(logical, "kv", secrets);
   //println(kv);
   return kv;
}

def _createJenkinsSecret(HashMap<String, String> secret, String domainName) {
   def keySets = secret.keySet();
   def domain = new Domain(domainName, null, Collections.<DomainSpecification>emptyList())
   for(iKv = 0; iKv < keySets.size(); iKv++) {
      secretText = new StringCredentialsImpl(CredentialsScope.GLOBAL, keySets[iKv], keySets[iKv], Secret.fromString(secret.get(keySets[iKv])));
      SystemCredentialsProvider.instance.store.addCredentials(domain, secretText);
   }
}

def _syncCredStore() {
   ArrayList<HashMap<String, HashMap<String, String>>> secrets = getAllSecrets();
   for(int iSecret = 0; iSecret < secrets.size(); iSecret++) {
       def keySets = secrets[iSecret].keySet();
       for(int iKvl = 0; iKvl < keySets.size(); iKvl++) {
          def domainName = keySets[iKvl].split('/')[0];
          _createJenkinsSecret(secrets[iSecret].getAt(keySets[iKvl]), domainName);
       }
   }
}

def _delCredStore() {
   ArrayList<HashMap<String, HashMap<String, String>>> secrets = getAllSecrets();
   for(int iSecret = 0; iSecret < secrets.size(); iSecret++) {
       def keySets = secrets[iSecret].keySet();
       for(int iKvl = 0; iKvl < keySets.size(); iKvl++) {
          def domainName = keySets[iKvl].split('/')[0];
	  def credentialsStore = jenkins.model.Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()
          def domain = new Domain(domainName, null, Collections.<DomainSpecification>emptyList())
          def allCreds = credentialsStore.getCredentials(domain)
          allCreds.each{
            credentialsStore.removeCredentials(domain, it)
          }
       }
   }
}

def syncCredStore() {
   _delCredStore();
   _syncCredStore();
}

//syncCredStore();
